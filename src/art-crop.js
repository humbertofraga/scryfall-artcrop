function addImageLink(classname, uri) {
    var card_actions = document.body.getElementsByClassName("card-actions")[0];
    var crop_uri = uri.replace("large", "art_crop")

    var art_crop_link = document.createElement("a");
    art_crop_link.className = "button-n";
    art_crop_link.href = crop_uri;

    var icon = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    icon.setAttribute("focusable", "false");
    icon.setAttribute("aria-hidden", "true");
    icon.setAttribute("viewBox", "0 0 24 24");
    var path = document.createElementNS("http://www.w3.org/2000/svg", "path");
    path.setAttribute("d", "M7,17V1H5V5H1V7H5V17A2,2 0 0,0 7,19H17V23H19V19H23V17M17,15H19V7C19,5.89 18.1,5 17,5H9V7H17V15Z");
    icon.appendChild(path);
    art_crop_link.appendChild(icon);

    var re_face = new RegExp("^.*-(.*)");
    var face = re_face.exec(classname)
    var b = document.createElement("b");
    b.textContent = "Art " + face[1];
    art_crop_link.appendChild(b);

    card_actions.appendChild(art_crop_link);

}

var card_images = document.body.getElementsByClassName("card-image")[0].children;

var image_uris = new Array();
for (index in card_images) {
    var classname = card_images[index].classList[0];
    var img = card_images[index].children[0];
    addImageLink(classname, img.getAttribute("src"))
}

